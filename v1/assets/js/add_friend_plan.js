/**
 * Created by jiang on 2019/1/31.
 */

var selectGroup;
var removeGroup;
var getExistGroup;
var checkExistGroup;

var selectMessageGroup;
var removeMessageGroup;
var getExistMessageGroup;
var checkExistMessageGroup;

var selectCustomerSource;
var removeCustomerSource;
var getExistCustomerSource;
var checkExistCustomerSource;

var goToUploadPage = false;
var setProcessStatus;

layui.use(['form'], function () {
    var $ = layui.$
        , form = layui.form;

    form.verify({
        partner_id: function (value) {
            if (value < 1) {
                return '请选择客户';
            }
        },
        name: function (value) {
            if (value.length < 0) {
                return '请输入计划名称';
            }
        },
        start_hour: function (value) {
            if (value < 0) {
                return '请选择开始时间';
            }
        },
        end_hour: function (value) {
            var start_hour = $('select[name="start_hour"]:selected').val();
            if (value < start_hour) {
                return '请选择合理的结束时间';
            }
        }
    });

    //监听提交
    form.on('submit(formSubmit)', function (data) {
        var hasCustomerSource = getExistCustomerSource();
        if (!hasCustomerSource) {
            layer.msg('请选择执行计划的数据表格', {icon: 5});
            return false
        }
        var messageType = $('input:radio[name="messageType"]:checked').val();
        if (messageType == 1) {
            var content = $('input[name="content"]').val();
            if (content == '' || content.trim() == '') {
                layer.msg('请填写验证消息', {icon: 5});
                $('input[name="content"]').focus();
                return false
            }
        } else if (messageType == 2) {
            var hasMessageGroup = getExistMessageGroup();
            if (!hasMessageGroup) {
                layer.msg('请选择执行计划的验证消息组', {icon: 5});
                return false
            }
        }
        var hasGroup = getExistGroup();
        if (!hasGroup) {
            layer.msg('请选择执行计划的微信组', {icon: 5});
            return false
        }
        layer.msg('提交中', {
            shadeClose: false
            , icon: 16
            , shade: 0.01
        });
        $.ajax({
            type: "POST",
            url: "/add_friend_plan/save",
            data: data.field,
            success: function (response) {
                var dataObj = $.parseJSON(response);
                if (dataObj.code === 0) {
                    layer.msg('数据提交成功', {icon: 6});
                    var targetUrl = '/add_friend_plan/index';
                    if (dataObj.planId && dataObj.planId > 0 && goToUploadPage == true) {
                        targetUrl = '/add_friend_plan/source/' + dataObj.planId;
                    }
                    setTimeout(function () {
                        location.href = targetUrl;
                    }, 1000);
                } else {
                    layer.msg(dataObj.msg, {icon: 5});
                }
            },
            error: function (request, status, error) {
                layer.msg('哦噢，网络开小差了', {icon: 5});
            }
        });
        return false;
    });

    form.on('radio(switchMessageType)', function (data) {
        var typeId = data.value;
        if (typeId == 1) {
            $('#single_message').css('display', 'block');
            $('#many_message').css('display', 'none');
        } else if (typeId == 2) {
            $('#single_message').css('display', 'none');
            $('#many_message').css('display', 'block');
        } else if (typeId == 3) {
            $('#single_message').css('display', 'none');
            $('#many_message').css('display', 'none');
        }
    });

    //Bof -- 微信组
    selectGroup = function (groupId) {
        var group = $('#availableGroup' + groupId);
        var newGroupName = filterHTMLTag(group.html());
        var html = '<button id="selectedGroup' + groupId + '" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="removeGroup(' + groupId + ');">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-close-fill" style="color: orange;"></i><input type="hidden" name="group_id[]" class="existGroup" value="' + groupId + '"></button>';
        $('#exist_group').append(html);
        group.remove();
        checkExistGroup();
    };

    removeGroup = function (groupId) {
        if (groupId > 0) {
            var group = $('#selectedGroup' + groupId);
            var newGroupName = filterHTMLTag(group.html());
            var html = '<button class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="selectGroup(' + groupId + ');" id="availableGroup' + groupId + '">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-add-circle" style="color: green;"></i></button>';
            $('#availableGroups').prepend(html);
            group.remove();
        }
        checkExistGroup();
    };

    getExistGroup = function () {
        // var objs = $('.existGroup');
        var objs = $('.wechatGroup:checked');
        var hasGroup = false;
        $.each(objs, function (index, obj) {
            hasGroup = true;
        });
        return hasGroup;
    };

    checkExistGroup = function () {
        var hasGroup = getExistGroup();
        if (hasGroup == true) {
            $('#no_group').css('display', 'none');
        } else {
            $('#no_group').css('display', 'block');
        }
    };
    //Eof -- 微信组

    //Bof -- 验证消息组
    selectMessageGroup = function (groupId) {
        var group = $('#availableMessageGroup' + groupId);
        var newGroupName = filterHTMLTag(group.html());
        var html = '<button id="selectedMessageGroup' + groupId + '" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="removeMessageGroup(' + groupId + ');">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-close-fill" style="color: orange;"></i><input type="hidden" name="message_group_id[]" class="existMessageGroup" value="' + groupId + '"></button>';
        $('#exist_message_group').append(html);
        group.remove();
        checkExistMessageGroup();
    };

    removeMessageGroup = function (groupId) {
        if (groupId > 0) {
            var group = $('#selectedMessageGroup' + groupId);
            var newGroupName = filterHTMLTag(group.html());
            var html = '<button class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="selectMessageGroup(' + groupId + ');" id="availableMessageGroup' + groupId + '">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-add-circle" style="color: green;"></i></button>';
            $('#availableMessageGroups').prepend(html);
            group.remove();
        }
        checkExistMessageGroup();
    };

    getExistMessageGroup = function () {
        // var objs = $('.existMessageGroup');
        var objs = $('.messageGroup:checked');
        var hasGroup = false;
        $.each(objs, function (index, obj) {
            hasGroup = true;
        });
        return hasGroup;
    };

    checkExistMessageGroup = function () {
        var hasGroup = getExistMessageGroup();
        if (hasGroup == true) {
            $('#no_message_group').css('display', 'none');
        } else {
            $('#no_message_group').css('display', 'block');
        }
    };
    //Eof -- 验证消息组

    //Bof -- 数据表格
    selectCustomerSource = function (groupId) {
        var group = $('#availableCustomerSource' + groupId);
        var newGroupName = filterHTMLTag(group.html());
        var html = '<button id="selectedCustomerSource' + groupId + '" class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="removeCustomerSource(' + groupId + ');">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-close-fill" style="color: orange;"></i><input type="hidden" name="customer_source_id[]" class="existCustomerSource" value="' + groupId + '"></button>';
        $('#exist_customer_source').append(html);
        group.remove();
        checkExistCustomerSource();
    };

    removeCustomerSource = function (groupId) {
        if (groupId > 0) {
            var group = $('#selectedCustomerSource' + groupId);
            var newGroupName = filterHTMLTag(group.html());
            var html = '<button class="layui-btn layui-btn-sm layui-btn-radius layui-btn-primary" onclick="selectCustomerSource(' + groupId + ');" id="availableCustomerSource' + groupId + '">' + newGroupName + '&nbsp;<i class="layui-icon layui-icon-add-circle" style="color: green;"></i></button>';
            $('#availableCustomerSources').prepend(html);
            group.remove();
        }
        checkExistCustomerSource();
    };

    getExistCustomerSource = function () {
        // var objs = $('.existCustomerSource');
        var objs = $('.customerSource:checked');
        var hasGroup = false;
        $.each(objs, function (index, obj) {
            hasGroup = true;
        });
        return hasGroup;
    };

    checkExistCustomerSource = function () {
        var hasGroup = getExistCustomerSource();
        if (hasGroup == true) {
            $('#no_customer_source').css('display', 'none');
        } else {
            $('#no_customer_source').css('display', 'block');
        }
    };
    //Eof -- 数据表格

    setProcessStatus = function (flag) {
        var processStatus = 2;
        if (flag == true) {
            processStatus = 1;
        }
        $('#process_status').val(processStatus);
    };

    //监听状态操作
    form.on('switch(statusDemo)', function (obj) {
        var status = 0;
        if (obj.elem.checked) {
            status = 1;
        }
        var id = this.step;
        var post = {
            id: id,
            status: status
        };
        $.ajax({
            type: "POST",
            url: "/add_friend_plan/change_status",
            data: post,
            success: function (response) {
                var dataObj = $.parseJSON(response);
                if (dataObj.code == 0) {
                    layer.tips('修改成功', obj.othis);
                } else if (dataObj.code == -1) {
                    layer.tips(dataObj.msg, obj.othis);
                } else {
                    layer.msg(dataObj.msg, {icon: 5});
                }
            },
            error: function (request, status, error) {

            }
        });
        return false;
    });
});

var filterHTMLTag = function (msg) {
    msg = msg.replace(/<\/?[^>]*>/g, ''); //去除HTML Tag
    msg = msg.replace(/[|]*\n/, ''); //去除行尾空格
    msg = msg.replace(/&nbsp;/ig, ''); //去掉nbsp
    return msg;
};

var setNext = function (flag) {
    goToUploadPage = false;
    if (flag == true) goToUploadPage = true;
};
