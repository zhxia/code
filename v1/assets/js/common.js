//JavaScript代码区域
layui.use('element', function () {
    var $ = layui.$
        , element = layui.element;

    var autodivheight = function () { //函数：获取尺寸
        //获取浏览器窗口高度
        var winHeight = 0;
        if (window.innerHeight)
            winHeight = window.innerHeight;
        else if ((document.body) && (document.body.clientHeight))
            winHeight = document.body.clientHeight;
        //通过深入Document内部对body进行检测，获取浏览器窗口高度
        if (document.documentElement && document.documentElement.clientHeight)
            winHeight = document.documentElement.clientHeight;
        //DIV高度为浏览器窗口的高度
        var height = (winHeight - 60 - 43 - 10);
//            document.getElementById("LAY_wechat_content").style.height= height +"px";
        $('.layui-card-body').css('height', height);
        $('.layui-card-body').css('overflow-y', 'auto');
    };
    autodivheight();
    window.onresize = autodivheight;
});
