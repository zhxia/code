document.writeln('    <link rel="stylesheet" href="../assets/layui/css/layui.css" media="all">')
document.writeln('    <link rel="stylesheet" href="../assets/style/login.css" media="all">')
document.writeln('    <link rel="stylesheet" href="../assets/style/admin.css">')
document.writeln('    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->')
document.writeln('    <!--[if lt IE 9]>')
document.writeln('    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>')
document.writeln('    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>')
document.writeln('    <![endif]-->')
document.writeln('    <script src="../assets/layui/layui.js"></script>')
document.writeln('    <script src="../assets/js/common.js"></script>')
document.writeln('    <div class="layui-header">')
document.writeln('    <div class="layui-logo"></div>')
document.writeln('    <!-- 头部区域（可配合layui已有的水平导航） -->')
document.writeln('    <ul class="layui-nav layui-layout-left">')
document.writeln('        <li class="layui-nav-item"><a href="../../v1/cs_user/index.html">客服管理</a></li>')
document.writeln('        <li class="layui-nav-item"><a href="javascript:;">微信账号</a>')
document.writeln('            <dl class="layui-nav-child">')
document.writeln('                <dd><a href="../../v1/wechat/index.html">账号列表</a></dd>')
document.writeln('                <dd><a href="../../v1/group/index.html">微信分组</a></dd>')
document.writeln('                <dd><a href="../../v1/wechat_import/index.html">A16和62批量导入</a></dd>')
document.writeln('                <dd><a href="../../v1/wechat_rule/batchEdit.html">批量设置加人规则</a></dd>')
document.writeln('                <dd><a href="../../v1/batch_change_wechat_info/index.html">批量修改微信信息</a></dd>')
document.writeln('                <dd><a href="../../v1/log/userActionLog.html">访问日志</a></dd>')
document.writeln('            </dl>')
document.writeln('        </li>')
document.writeln('        <li class="layui-nav-item"><a href="javascript:;">好友管理</a>')
document.writeln('            <dl class="layui-nav-child">')
document.writeln('                <dd><a href="../../v1/customer_source/add.html">导入数据加人引导</a></dd>')
document.writeln('                <dd><a href="../../v1/customer_source/index.html">导入数据管理</a></dd>')
document.writeln('                <dd><a href="../../v1/message/groupIndex.html">验证消息管理</a></dd>')
document.writeln('                <dd><a href="../../v1/add_friend_plan/index.html">加人计划管理</a></dd>')
document.writeln('                <dd><a href="../../v1/add_friend_history/index.html">加人历史记录</a></dd>')
document.writeln('                <dd><a href="../../v1/sns_post/index.html">朋友圈素材</a></dd>')
document.writeln('                <dd><a href="../../v1/sns_post_plan/index.html">朋友圈任务</a></dd>')
document.writeln('            </dl>')
document.writeln('        </li>')
document.writeln('        <li class="layui-nav-item"><a href="javascript:;">素材库</a>')
document.writeln('            <dl class="layui-nav-child">')
document.writeln('                <dd><a href="../../v1/wechat/addGroupSendInfo.html">群发信息</a></dd>')
document.writeln('                <dd><a href="../../v1/material/index.html">聊天素材</a></dd>')
document.writeln('                <dd><a href="../../v1/fake_talk_material/index.html">养号素材</a></dd>')
document.writeln('                <dd><a href="../../v1/user_wechat_nickname/index.html">微信昵称素材</a></dd>')
document.writeln('                <dd><a href="../../v1/user_wechat_headimg/index.html">微信头像素材</a></dd>')
document.writeln('                <dd><a href="../../v1/user_wechat_sign/index.html">微信个性签名素材</a></dd>')
document.writeln('                <dd><a href="../../v1/user_wechat_snsimg/index.html">朋友圈背景图素材</a></dd>')
document.writeln('                <dd><a href="../../v1/auto_reply/index.html">自动回复设置</a></dd>')
document.writeln('            </dl>')
document.writeln('        </li>')
document.writeln('        <li class="layui-nav-item"><a href="../../v1/report/index.html">统计</a></li>')
document.writeln('    </ul>')
document.writeln('    <ul class="layui-nav layui-layout-right">')
document.writeln('        <li class="layui-nav-item"><a href="javascript:;">账号XXX</a>')
document.writeln('            <dl class="layui-nav-child">')
document.writeln('                <dd><a href="../../v1/account/index.html">基本资料</a></dd>')
document.writeln('                <dd><a href="../../v1/account/changePassword.html">修改密码</a></dd>')
document.writeln('                <dd><a href="../../v1/account/setProtectPassword.html">设置二级密码</a></dd>')
document.writeln('            </dl>')
document.writeln('        </li>')
document.writeln('        <li class="layui-nav-item layui-hide-xs"><a href="../../v1/login/login.html">退出</a></li>')
document.writeln('    </ul>')
document.writeln('</div>')
