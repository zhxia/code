layui.use('element', function () {
    var $ = layui.$
        , element = layui.element;

    var autoDivHeight = function () { //函数：获取窗口尺寸
        //获取浏览器窗口高度
        var winHeight = 0;
        if (window.innerHeight)
            winHeight = window.innerHeight;
        else if ((document.body) && (document.body.clientHeight))
            winHeight = document.body.clientHeight;
        //通过深入Document内部对body进行检测，获取浏览器窗口高度
        if (document.documentElement && document.documentElement.clientHeight)
            winHeight = document.documentElement.clientHeight;
        //DIV高度为浏览器窗口的高度
        var height = (winHeight - 60 - 43 - 30);
        $('.layui-card-body').css({'height': height, 'overflow-y': 'auto'});
    };
    autoDivHeight();
    window.onresize = autoDivHeight;
});

/**
 * 把时间戳转换为YYYY-MM-DD HH:MM:SS 格式
 */
var parseTime = function (time, isDate) {
    if (time) {
        const date = new Date(time * 1000)
        const year = date.getFullYear()
        /* 在日期格式中，月份是从0开始的，因此要加0
         * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
         * */
        const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
        const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
        const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
        const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
        const seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
        // 拼接
        if (isDate) { // YYYY-MM-DD 格式
            return year + '-' + month + '-' + day
        } else { // YYYY-MM-DD HH:mm:ss 格式
            return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
        }
    } else {
        return ''
    }
}

// 解析微信的表情消息
var parseEmoji = function (content, right) {
    content = content.split(' = ')
    var c = [];
    var url = ''
    var f = new Promise((resolve, reject) => {
        for (let i = 0; i < content.length; i++) {
            if (content[i].indexOf('cdnurl') !== -1) {
                c = content[i + 1]
                url = c.slice(1, c.lastIndexOf('"'))
                resolve(url)
                break
            }
        }
    });
    return `<img src='${url}?${Math.random()}' style="height:100px;margin-top: 10px" />`;
}

// 获取带有双字节字符的长度
String.prototype.dbLength = function () {
    var str = this, leg = str.length;
    for (var i in str) {
        if (str.hasOwnProperty(i)) {
            var db = str[i].charCodeAt(0).toString(16).length == 4;
            if (db) leg += 1;
        }
    }
    return leg;
}
